<div class="sidebar" data-background-color="tmontec-dark">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="{{ asset('imagens/logo-branco.png') }}" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                            <span>
                                {{ \Illuminate\Support\Facades\Auth::user()->name }}
                                <span class="user-level">Administrator</span>
                                <span class="caret"></span>
                            </span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse in" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="#profile">
                                    <span class="link-collapse">My Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="#edit">
                                    <span class="link-collapse">Edit Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="#settings">
                                    <span class="link-collapse">Settings</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav nav-primary">
                <li class="nav-item">
                    <a data-toggle="collapse" href="#dashboard" class="collapsed" aria-expanded="false">
                        <i class="fas fa-home"></i>
                        <p>Dashboard</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="dashboard">
                        <ul class="nav nav-collapse">
                            <li>
                                <a href="../demo1/index.html">
                                    <span class="sub-item">Dashboard 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="../demo2/index.html">
                                    <span class="sub-item">Dashboard 2</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
                    <h4 class="text-section">Components</h4>
                </li>
                <li class="nav-item active submenu">
                    <a data-toggle="collapse" href="#submenu">
                        <i class="fas fa-bars"></i>
                        <p>Menu Levels</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="submenu">
                        <ul class="nav nav-collapse">
                            <!-- fornecedor -->
                            <li>
                                <a data-toggle="collapse" href="#fornecedor">
                                    <span class="sub-item">Fornecedor</span>
                                    <span class="caret"></span>
                                </a>
                                <div class="collapse" id="fornecedor">
                                    <ul class="nav nav-collapse subnav">
                                        <li>
                                            <a href="{{route('fornecedores.index')}}">
                                                <span class="sub-item">Listagem</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('fornecedores.create')}}">
                                                <span class="sub-item">Cadastro</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- mercadoria -->
                            <li>
                                <a data-toggle="collapse" href="#mercadoria">
                                    <span class="sub-item">Mercadoria</span>
                                    <span class="caret"></span>
                                </a>
                                <div class="collapse" id="mercadoria">
                                    <ul class="nav nav-collapse subnav">
                                        <li>
                                            <a href="{{route('mercadorias.index')}}">
                                                <span class="sub-item">Listagem</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('mercadorias.create')}}">
                                                <span class="sub-item">Cadastro</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- grupo -->
                            <li>
                                <a data-toggle="collapse" href="#grupo">
                                    <span class="sub-item">Grupo</span>
                                    <span class="caret"></span>
                                </a>
                                <div class="collapse" id="grupo">
                                    <ul class="nav nav-collapse subnav">
                                        <li>
                                            <a href="{{route('grupos.index')}}">
                                                <span class="sub-item">Listagem</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('grupos.create')}}">
                                                <span class="sub-item">Cadastro</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>

                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
